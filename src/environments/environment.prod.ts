const apiurl = 'https://private-anon-11b2c39961-henrybravo.apiary-mock.com/';
const apiurl2 = 'https://private-anon-022b4ce9be-henrybravo.apiary-mock.com/';

export const environment = {
  production: true,
  register: apiurl + 'register',
  login: apiurl + 'login',
  user: apiurl + 'user',
  createLink: apiurl2 + 'links',
};
