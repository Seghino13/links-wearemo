import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpClient, HttpInterceptor } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class InterceptorService implements HttpInterceptor {

  constructor(public http: HttpClient) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const currentUser = JSON.parse( String(localStorage.getItem('session')));
     if (currentUser) {
      request = request.clone({
        setHeaders: {
            Token: currentUser.token,
            'Content-Type':'application/json'
        },
      });
    } 
    
    return next.handle(request).pipe(
      catchError(erro => {
        if (erro.status === 401) {
         
        }
        return throwError(erro);
      })
    );
  }
}
