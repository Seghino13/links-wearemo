import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IAuth } from '../interfaces/IAuth';
import { ILoginResponse } from '../interfaces/ILoginResponse';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor( private http: HttpClient ) { }

  async register( signup: IAuth ){
    const req =  await this.http.post( environment.register, signup).toPromise();
    return req;
  }

  async login( login: IAuth ){
    const req =  await this.http.post<ILoginResponse>( environment.login, login).toPromise();
    return req;
  }
}
