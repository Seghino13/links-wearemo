import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ILinks } from '../interfaces/ILinks';

@Injectable({
  providedIn: 'root'
})
export class LinksService {

  constructor( private http: HttpClient ) { }

  async createLinks( links: ILinks ){
    const req = await this.http.post( environment.createLink, links ).toPromise();
    return req;
  }

  async listLinks(){
    const req = await this.http.get( environment.createLink ).toPromise();
    return req;
  }

  async deleteLinks( links: ILinks){
    const req = await this.http.delete( environment.createLink + '/'+ links.id ).toPromise();
    return req;
  }

  
}
