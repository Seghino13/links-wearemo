import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ILinks } from '../interfaces/ILinks';
import { ILoginResponse } from '../interfaces/ILoginResponse';

@Injectable({
  providedIn: 'root'
})
export class HelpersService {

  constructor(private toastr: ToastrService) { }

  success( title:string, subTitle:string ){
    this.toastr.success(subTitle, title);
  }

  error( title:string, subTitle:string ){
    this.toastr.error(subTitle, title);
  }

  login( login: ILoginResponse ){
    localStorage.setItem('session', JSON.stringify(login) );
  }

  getSession(){
    return localStorage.getItem('session');
  }

  saveLink( links:ILinks[] ){
    localStorage.setItem( 'links', JSON.stringify(links));
  }
  getLinks(){
    return localStorage.getItem('links');
  }
  clearLocalStorage(){
    localStorage.clear();
  }
}
