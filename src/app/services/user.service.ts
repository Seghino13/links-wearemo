import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { IUser } from '../interfaces/IUser';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor( private http: HttpClient ) { }

  async getUser( id: string ){
    const req =  await this.http.get<IUser>( environment.user +'/'+ id).toPromise();
    return req;
  }
}
