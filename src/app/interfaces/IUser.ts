export interface IUser {
    id:string,
    createdAt: Date,
    name:string,
    email:string,
    avatar:string
}