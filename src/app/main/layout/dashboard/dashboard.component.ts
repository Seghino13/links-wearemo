import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HelpersService } from 'src/app/services/helpers.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers:[HelpersService]
})
export class DashboardComponent {
  logo:string = '../../../../assets/images/logo.svg';

  button = {
    label: 'LOGOUT',
  };
  constructor(private helpersService:HelpersService, private router:Router) { }

  logout(){
    this.helpersService.clearLocalStorage();
    this.router.navigate(['/login']);

  }

}
