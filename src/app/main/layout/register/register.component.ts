import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  logo:string = '../../../../assets/images/logo.svg';

  button = {
    label: 'SIGNUP',
    routerLink: '/signup'
  };

  constructor( private router:Router ) { }

  ngOnInit() {
    if( this.router.routerState.snapshot.url === '/signup'){
      this.button = {
        label: 'LOGIN',
        routerLink: '/login' 
      }
    }else{
      this.button = {
        label: 'SIGNUP',
        routerLink: '/signup' 
      }
    }
  }

}
