import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RoutesLink } from 'src/app/enums/routes';
import { IAuth } from 'src/app/interfaces/IAuth';
import { AuthService } from 'src/app/services/auth.service';
import { HelpersService } from 'src/app/services/helpers.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss'],
  providers:[AuthService, HelpersService]
})
export class SignupComponent implements OnInit {

  signup: FormGroup | undefined;
  send: boolean = false;
  constructor(  private authService:AuthService, 
                private helpersService:HelpersService,
                private router: Router) { }

  ngOnInit() {
    this.signup = new FormGroup({
      fullName: new FormControl('', [Validators.required, Validators.minLength(2)]),
      email: new FormControl('',[ Validators.email, Validators.required ]),
      password: new FormControl('', [Validators.required]),
    });
  }

  get fullName() { return this.signup?.get('fullName'); }
  get email() { return this.signup?.get('email'); }
  get password() { return this.signup?.get('password'); }

  async submit(){
    if( !this.signup?.valid){
      return
    }
    this.send = true;
    try{
      const data: IAuth = {
        name: this.signup.value.fullName,
        email: this.signup.value.email,
        password: this.signup.value.password
      } 
      const request = await this.authService.register( data );
      this.helpersService.success('Register Success!!','Try to login');
      this.signup.reset();
      this.router.navigate(['/'+RoutesLink.login]);
      
    }catch (e) {
      console.log('@Error => ',e);
      this.helpersService.error('Oppps error','Try again');
    }
    this.send = false;
  }
  
}
