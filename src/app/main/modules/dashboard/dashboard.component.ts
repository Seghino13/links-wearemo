import { Component, OnInit } from '@angular/core';
import { ILinks } from 'src/app/interfaces/ILinks';
import { IUser } from 'src/app/interfaces/IUser';
import { HelpersService } from 'src/app/services/helpers.service';
import { LinksService } from 'src/app/services/links.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [UserService, HelpersService, LinksService]
})
export class DashboardComponent implements OnInit {

  constructor(  private userService: UserService, 
                private helpersService:HelpersService,
                private linksService:LinksService) { }
  user: IUser | undefined;
  links: ILinks[] = [];
  ngOnInit() {
    this.getUser();
    this.getLinks();
  }

  async getUser(){
    try{
      const session = JSON.parse( String(this.helpersService.getSession()) );
      const user = await this.userService.getUser(session.id);
      this.user = user;
    }catch (e) {
      console.log('@Error =>', e );
    }
  }

  async getLinks(){
    try{
      const req = await this.linksService.listLinks();
      
    }catch(e:any){
      console.log('@Error =>', e);
      this.helpersService.error('Opppss!!', 'Service unavailable, please try again');
      this.listToLocalStorage();
    }
  }

  listToLocalStorage(){
    const links = this.helpersService.getLinks();
    this.links = JSON.parse(String(links));
    console.log(this.links);
  }

  emitSaveForm(){
   this.getLinks();
  }

}
