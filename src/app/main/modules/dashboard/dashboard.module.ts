import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { ProfileComponent } from './profile/profile.component';
import { LinkFormComponent } from './link-form/link-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ListLinkComponent } from './list-link/list-link.component';


@NgModule({
  declarations: [
    DashboardComponent,
    ProfileComponent,
    LinkFormComponent,
    ListLinkComponent,
    
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    
  ]
})
export class DashboardModule { }
