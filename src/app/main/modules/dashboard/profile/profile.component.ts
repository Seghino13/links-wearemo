import { Component, Input, OnInit } from '@angular/core';
import { ILoginResponse } from 'src/app/interfaces/ILoginResponse';
import { IUser } from 'src/app/interfaces/IUser';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  @Input() item: IUser | undefined ; 
  user: IUser | undefined;
  constructor() { 
    
  }

  ngOnInit() {
    this.user = this.item;
  }

}
