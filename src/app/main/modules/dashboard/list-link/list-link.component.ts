import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ILinks } from 'src/app/interfaces/ILinks';
import { HelpersService } from 'src/app/services/helpers.service';
import { LinksService } from 'src/app/services/links.service';

@Component({
  selector: 'app-list-link',
  templateUrl: './list-link.component.html',
  styleUrls: ['./list-link.component.scss'],
  providers:[LinksService, HelpersService]
})
export class ListLinkComponent  {

  @Input() links: ILinks[] | undefined ; 
  imgDelete:string = '../../../../../assets/images/delete.svg';
  @Output() deleteLinkForm = new EventEmitter<string>();
  constructor( private linksService:LinksService, private helpersService:HelpersService ) { }

  async deleteLink( item:ILinks ){
    try{
      const del = await this.linksService.deleteLinks(item);
      this.deleteLinkForm.emit();
    }catch(e){
      this.helpersService.error('Opppss!!', 'Service unavailable, please try again');
      console.log('@Error', e);
      this.deleteTolocalStorage( item );
    }
  }

  deleteTolocalStorage( item:ILinks ){
    const getLinks = this.helpersService.getLinks();
      if( getLinks !== null ){
        const parse:ILinks[] = JSON.parse(getLinks);
        console.log( parse );
        this.helpersService.saveLink( parse.filter( x => x.id !== item.id ) );
        this.deleteLinkForm.emit();
      }
      
  }
}
