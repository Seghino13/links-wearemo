import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ILinks } from 'src/app/interfaces/ILinks';
import { HelpersService } from 'src/app/services/helpers.service';
import { LinksService } from 'src/app/services/links.service';

@Component({
  selector: 'app-link-form',
  templateUrl: './link-form.component.html',
  styleUrls: ['./link-form.component.scss'],
  providers:[LinksService, HelpersService]
})
export class LinkFormComponent implements OnInit {
  link: FormGroup | undefined;
  send: boolean = false;
  @Output() saveLinkForm = new EventEmitter<string>();
  constructor( private linksService:LinksService, private helpersService:HelpersService,) { }

  ngOnInit() {
    this.link = new FormGroup({
      url: new FormControl('',[ Validators.required ]),
      name: new FormControl('', [Validators.required]),
    });
  }

  get url() { return this.link?.get('url'); }
  get name() { return this.link?.get('name'); }

  async submit(){
    if( !this.link?.valid){
      return
    }
    this.send = true;
    const data: ILinks = {
      name:  this.link.value.name,
      url:  this.link.value.url,
    }
    try{
      const request = await this.linksService.createLinks( data );
      console.log(request);

    }catch (e) {

      console.log('@Error => ',e);
      this.helpersService.error('Opppss!!', 'Service unavailable, please try again');

      const getLinks = this.helpersService.getLinks();
      console.log(getLinks);
      if( getLinks !== null && !getLinks && getLinks.length > 0){

        const parse = JSON.parse(getLinks);
        
        data.id = String( Number( parse[ parse.length - 1 ].id ) + 1 )

        parse.push( data );

        this.helpersService.saveLink(parse);


      }else{
        data.id = '1';
        this.helpersService.saveLink([data]);
      }
    }
    this.send = false;
    this.saveLinkForm.emit();
    this.link.reset();
  }

}
