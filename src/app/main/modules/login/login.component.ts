import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RoutesLink } from 'src/app/enums/routes';
import { IAuth } from 'src/app/interfaces/IAuth';
import { AuthService } from 'src/app/services/auth.service';
import { HelpersService } from 'src/app/services/helpers.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers:[AuthService, HelpersService]
})
export class LoginComponent implements OnInit {

  login: FormGroup | undefined;
  send: boolean = false;
  constructor(  private authService:AuthService, 
                private helpersService:HelpersService,
                private router: Router) { }

  ngOnInit() {
    this.login = new FormGroup({
      email: new FormControl('',[ Validators.email, Validators.required ]),
      password: new FormControl('', [Validators.required]),
    });
  }

  get email() { return this.login?.get('email'); }
  get password() { return this.login?.get('password'); }

  async submit(){
    if( !this.login?.valid){
      return
    }
    this.send = true;
    try{
      const data: IAuth = {
        email: this.login.value.email,
        password: this.login.value.password
      } 
      const request = await this.authService.login( data );
      this.helpersService.success('Login Successfully!!','');
      request.id = '1';
      this.helpersService.login(request);
      this.login.reset();
      this.router.navigate(['/'+RoutesLink.dashborad]);
      
    }catch (e) {
      console.log('@Error => ',e);
      this.helpersService.error('Error login','Try again');
    }
    this.send = false;
  }

}
