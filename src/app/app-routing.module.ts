import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoutesLink } from './enums/routes';
import { SessionGuard } from './guards/session.guard';
import { DashboardComponent } from './main/layout/dashboard/dashboard.component';
import { RegisterComponent } from './main/layout/register/register.component';

const routes: Routes = [
  {
    path: RoutesLink.login,
    component: RegisterComponent,
    loadChildren: () => import('./main/modules/login/login.module').then(m => m.LoginModule),
   // canActivate:[SessionGuard]
  },
  {
    path: RoutesLink.signup,
    component: RegisterComponent,
    loadChildren: () => import('./main/modules/signup/signup.module').then(m => m.SignupModule)
  },
  {
    path: RoutesLink.dashborad,
    component: DashboardComponent,
    loadChildren: () => import('./main/modules/dashboard/dashboard.module').then(m => m.DashboardModule)
  },
  {
    path: '',
    redirectTo: '/'+RoutesLink.login,
    pathMatch: 'full' 
  }
  
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
